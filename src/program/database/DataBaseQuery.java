package program.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataBaseQuery {
	
	
	public  static ResultSet SelectFrom(Statement st, String tabela){
		ResultSet rSet = null ;
		try {
			rSet = st.executeQuery("Select * from "+ tabela+ ";");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Problem z zapytaniem");
		}
		return rSet;
				
	}

	public static int UpdateQuery(Statement st, String Query) throws SQLException{
		int update =0;
		
			update = st.executeUpdate(Query);
	
			
		
		return update;
		
	}
	
	public  static ResultSet Select(Statement st, String query){
		ResultSet rSet = null ;
		try {
			rSet = st.executeQuery(query);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Problem z zapytaniem");
		}
		return rSet;
				
	}
	
	public static void DbUpdateViews(Statement ReadWriteStatment, Statement ReadOnlyStatment){
		int resultUpdate = 0;
		ResultSet resultSet;
		try {
			resultUpdate = DataBaseQuery.UpdateQuery(ReadWriteStatment, 
					"if exists (select 1 "+
				            "from  sysobjects "+
				           "where  id = object_id('go��_view') "+
				            ") "+
				   " drop view go��_view ");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(resultUpdate);
		
		try {
			resultUpdate = DataBaseQuery.UpdateQuery(ReadWriteStatment, 
				" create view go��_view as"+
					 " (select d.Nazwa_klubu as Go��, g1.ID_dru�yny, m.ID_meczu as mecz,  Kolejka as Kolejka,"+ 
					" CONVERT(date,m.Data_meczu,120) as 'Data Spotkania',g1.Wynik as 'Wynik go��'"+
					" from"+
					" mecze m, Go�� g1, Gospodarz g2,DRU�YNY d"+
					" where m.ID_meczu =  g1.ID_meczu and"+ 
					" m.ID_meczu = g2.ID_meczu and(d.ID_dru�yny = g1.ID_dru�yny ))"
			        );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			resultUpdate = DataBaseQuery.UpdateQuery(ReadWriteStatment, 
					"if exists (select 1 "+
				            "from  sysobjects "+
				           "where  id = object_id('strzelcy') "+
				            ") "+
				   "drop view strzelcy ");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			resultUpdate = DataBaseQuery.UpdateQuery(ReadWriteStatment, 
					"Create view strzelcy as select p.Nazwisko as Nazwisko, "
					+ " p.Imie as Imi�, p.Pozycja as Pozycja,d.Nazwa_klubu as klub, "
					+ " sum(z.gol) as bramka from "+
					" PI�KARZE p, ZDARZENIA z, DRU�YNY d "+
					" where p.ID_pi�karza = z.ID_pi�karza and p.ID_dru�yny = d.ID_dru�yny "+ 
					" group by p.Nazwisko, p.Imie , p.Pozycja, d.Nazwa_klubu"
				        );
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			resultUpdate = DataBaseQuery.UpdateQuery(ReadWriteStatment, 
					"if exists (select 1 "+
				            "from  sysobjects "+
				           "where  id = object_id('gospodarz_view') "+
				            ") "+
				   "drop view gospodarz_view ");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//	System.out.println(resultUpdate);
				
		try {
			resultUpdate = DataBaseQuery.UpdateQuery(ReadWriteStatment, 
				" Create view "+ 
					"gospodarz_view "+ 
					"as "+
					"(select d.Nazwa_klubu as Gospodarz,g2.ID_dru�yny, Kolejka as Kolejka, m.ID_meczu as mecze, "+
					"CONVERT(date,m.Data_meczu,120) as 'Data_Spotkania', g2.Wynik as 'Wynik gospodarz' "+
					"from "+
					"mecze m, Go�� g1, Gospodarz g2,DRU�YNY d "+ 
					"where m.ID_meczu =  g1.ID_meczu and "+ 
					"m.ID_meczu = g2.ID_meczu and(d.ID_dru�yny = g2.ID_dru�yny ) )"
			        );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	//	System.out.println(resultUpdate);
		
		
		
		
		
		
	}
	
	
}
