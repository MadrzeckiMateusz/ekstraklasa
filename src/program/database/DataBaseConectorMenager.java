package program.database;

import java.util.ArrayList;

public class DataBaseConectorMenager {
	
	private static  ArrayList<DataBaseConnector> connectorList = new ArrayList<DataBaseConnector>();
	private static  ArrayList<String> connectorClass = new ArrayList<String>();
	private static   int connectorId = 0;
	
	
	public  static int AddConector(String ClasName){
		connectorList.add( new DataBaseConnector(connectorId++));
		connectorClass.add(ClasName);
		
		return connectorId;
	}
	public  static DataBaseConnector GetConnection(int connectorId){
		
		return connectorList.get(connectorId-1);
	}
	public static void CloseAllConection(){
		for(DataBaseConnector con : connectorList)
			con.CloseConnection();
		
			
	}
}
