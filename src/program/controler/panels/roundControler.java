package program.controler.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.model.tables.ResultSetTableModel;
import program.view.panels.roundView;

public class roundControler {

	 private DataBaseConnector conector;
	 private int ConnectorId;
	 private Statement ReadOnlyStatment;
	 private Statement  ReadWriteStatment;
	 private ResultSet resultSet;
	 private ResultSet resultSet1;	
	 private ResultSetTableModel resultSetTableModel;
	 private roundView rView;
	 
	 private ArrayList<String> round;
	 
	 
	 
	public roundControler(){
		 ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
		 conector = DataBaseConectorMenager.GetConnection(ConnectorId);
		ReadOnlyStatment = conector.ReadOnlyConection();
		ReadWriteStatment = conector.ReadWriteConection();
		resultSetTableModel = new ResultSetTableModel();
		rView = new roundView();
		round = new ArrayList<String>();
		SelectAll();
		SetComoboxValues();
		Events();
	}
	
	public void SelectAll(){
		
		DataBaseQuery.DbUpdateViews(ReadWriteStatment, ReadOnlyStatment);
		
		resultSet = DataBaseQuery.Select(ReadOnlyStatment, 
				"SELECT g1.Gospodarz as Gospodarz, g1.[Wynik gospodarz] , g2.[Wynik go��], g2.Go�� as Go�� , g1.Kolejka "+ 
				"FROM gospodarz_view g1 "+
				"LEFT JOIN go��_view g2 "+
				"ON g1.mecze = g2.mecz "+
				"order by g1.kolejka ");
		resultSetTableModel.setResultSetTable(resultSet);
		rView.table.setModel(resultSetTableModel);
		rView.repaint();
		
	}
	public void FiltersResult(){
		String round = (String) rView.comboBox.getSelectedItem();
		if (!round.equals(" ")){
		resultSet = DataBaseQuery.Select(ReadOnlyStatment, 
				"SELECT g1.Gospodarz as Gospodarz, g1.[Wynik gospodarz] , g2.[Wynik go��], g2.Go�� as Go�� , g1.Kolejka "+ 
				"FROM gospodarz_view g1 "+
				"LEFT JOIN go��_view g2 "+
				"ON g1.mecze = g2.mecz "+
				"Where g1.kolejka ="+"'"+round+"'"+
				"order by g1.kolejka ");
		
		resultSetTableModel.setResultSetTable(resultSet);
		rView.table.setModel(resultSetTableModel);
		rView.repaint();
		}
		else
		SelectAll();
	}
	public void SetComoboxValues(){
		
		resultSet1 = DataBaseQuery.Select(ReadWriteStatment, "Select kolejka from mecze group by kolejka");
		
		try {
			round.add(" ");
			while (resultSet1.next() ){
			round.add(resultSet1.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		for (String s : round)
		rView.comboBox.addItem(s); 	
	}
	public void Events(){
		rView.btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				FiltersResult();
				
			}
		});
	}

	public roundView getrView() {
		return rView;
	}

	public void setrView(roundView rView) {
		this.rView = rView;
	}
}
