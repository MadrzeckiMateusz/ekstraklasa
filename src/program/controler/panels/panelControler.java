package program.controler.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.freixas.jcalendar.DateEvent;
import org.freixas.jcalendar.DateListener;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.model.tables.ResultSetTableModel;
import program.view.panels.PanelViev;

public class panelControler {
	
	 private DataBaseConnector conector;
	 private int ConnectorId;
	 private Statement ReadOnlyStatment;
	 private Statement  ReadWriteStatment;
	 private ResultSet resultSet;
	 private ResultSet resultSet1;	
	 private ResultSetTableModel resultSetTableModel;	 
	 private PanelViev view;
	 private ArrayList<String> teams;
	 private ArrayList<String> queeNumber;
	 private String date;
	
	public panelControler(){
		ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
		conector = DataBaseConectorMenager.GetConnection(ConnectorId);
		ReadOnlyStatment = conector.ReadOnlyConection();
		ReadWriteStatment = conector.ReadWriteConection();
		resultSetTableModel = new ResultSetTableModel();
		teams = new ArrayList<String>();
		queeNumber = new ArrayList<String>();
		view = new PanelViev();
		resultSet = null;
		resultSet1= null;
		date = new String(" ' ' ");
		SelectAll();
		Events();
		SetComoboxValues();	
		
		
		
		
	}
		public void SelectAll(){
			
			DataBaseQuery.DbUpdateViews(ReadWriteStatment, ReadOnlyStatment);
			
			resultSet = DataBaseQuery.Select(ReadOnlyStatment, 
					"SELECT g1.Gospodarz as Gospodarz, g1.[Wynik gospodarz] , g2.[Wynik go��], g2.Go�� as Go�� , g1.Data_Spotkania as 'Data Spotkania', g1.Kolejka "+
					"FROM gospodarz_view g1 "+
					"LEFT JOIN go��_view g2 "+
					"ON g1.mecze = g2.mecz "
					+ "order by g1.kolejka;");
			
			resultSetTableModel.setResultSetTable(resultSet);
			view.table.setModel(resultSetTableModel);
			view.repaint();
			
			StatusBarControler.SetStatusBarSatte("Wy�wietlenie danych pobranych z bazy danych");
			
		}
		
		public void FiltersResult(){
			String teams =  " '"+ (String) view.comboBox.getSelectedItem()+"' ";
			String round = " '"+(String) view.comboBox_1.getSelectedItem()+"' ";
			
			String datez = " ' ' ";
			if (teams.equals(" ' ' "))
				 teams="";
			 else
				 teams = " AND g1.gospodarz like "+ teams + " OR g2.go�� like "+teams+"" ;
			 if (round.equals(" ' ' "))
				 round="";
			 else
				 round= " AND g1.kolejka like "+round;
			 if (date.equals(" ' ' "))
				 datez = "";
			 else
			 { datez = " AND  g1.Data_Spotkania like "+ date;
			 	date = " ' ' ";
			 }
				
			 String Query = "WHERE g1.kolejka <> ' ' "+ teams+round+datez;
				resultSet =DataBaseQuery.Select(ReadOnlyStatment,
						"SELECT  g1.Gospodarz as Gospodarz, g1.[Wynik gospodarz] , g2.[Wynik go��], g2.Go�� as Go�� , g1.Data_Spotkania as 'Data Spotkania', g1.Kolejka "+
								"FROM gospodarz_view g1 "+
								"LEFT JOIN go��_view g2 "+
								"ON g1.mecze = g2.mecz "+
								Query
								+ " order by g1.kolejka;");
				
				resultSetTableModel.setResultSetTable(resultSet);
				view.table.setModel(resultSetTableModel);
				view.repaint();
				StatusBarControler.SetStatusBarSatte("Wy�wietlenie odfiltrowanych wynik�w");
			}
			
			
			
		

		public void SetComoboxValues(){
			
			resultSet1 = DataBaseQuery.Select(ReadWriteStatment, "Select Nazwa_klubu from dru�yny");
			
			try {
				
				teams.add(" ");
				while (resultSet1.next() ){
				teams.add(resultSet1.getString(1));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			resultSet1 = DataBaseQuery.Select(ReadWriteStatment, "Select Kolejka from mecze "+
											"group by kolejka");
			try {
				
				queeNumber.add(" ");
				while (resultSet1.next() ){
				queeNumber.add(resultSet1.getString(1));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				StatusBarControler.SetStatusBarSatte("Ustawienie danych w listach");
			}
			
			for (String s : teams)
			view.comboBox.addItem(s); 
			
			for (String s: queeNumber)
				view.comboBox_1.addItem(s);
			
		}
		
		public void Events(){
			view.btnNewButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					FiltersResult();
					
				}
			});
			view.calendarCombo.addDateListener(new DateListener() {
				
				@Override
				public void dateChanged(DateEvent arg0) {
					 date  = " '"+(String) view.calendarCombo.getSelectedItem()+"' ";
					
				}
			});
		}
		
		
		public PanelViev getView() {
			return view;
		}
		public void setView(PanelViev view) {
			this.view = view;
		}
	
	
}