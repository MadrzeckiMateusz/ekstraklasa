package program.controler.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.freixas.jcalendar.DateEvent;
import org.freixas.jcalendar.DateListener;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.model.tables.ResultSetTableModel;
import program.view.panels.GolKipperView;

public class GolKipperControler {
	
	
	private DataBaseConnector conector;
	 private int ConnectorId;
	 private Statement ReadOnlyStatment;
	 private Statement  ReadWriteStatment;
	 private ResultSet resultSet;
	 private ResultSetTableModel resultSetTableModel;
	 private ArrayList<String> GkFirstName;
	 private ArrayList<String> GkName;
	 private ArrayList<String> GkTeamName;
	 
	 private String BirthDate;
	 private String LooseGols;
	 
	 private GolKipperView golKipperView;
	 
	 public GolKipperControler(){
		 ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
			conector = DataBaseConectorMenager.GetConnection(ConnectorId);
			ReadOnlyStatment = conector.ReadOnlyConection();
			ReadWriteStatment = conector.ReadWriteConection();
			resultSetTableModel = new ResultSetTableModel();
			golKipperView = new GolKipperView();
			resultSet = null;
			BirthDate = " ' ' ";
			LooseGols = " ' ' ";
			
			GkFirstName = new ArrayList<String>();
			GkName = new ArrayList<String>();
			GkTeamName = new ArrayList<String>();
			
			
			GetValueForComobox();
			SelectAll();
			Events();
			
	 }
	 public void SelectAll(){
		 resultSet = DataBaseQuery.Select(ReadOnlyStatment, 
				 "Select p.nazwisko as 'Nazwisko',p.imie as Imi�, CONVERT(date,p.Data_ur,120) as 'Data urodzenia', "+
				 "p.Pozycja as 'Pozycja', d.Nazwa_klubu as Klub, sum(z.Strata) as 'Stracone Bramki', "+
				 "sum(z.Czerwona_kartka) as 'Czerwone kartki', sum(z.��ta_kartka) as '��te kartki' "+
				 "from PI�KARZE p "+
				 "left join ZDARZENIA z "+ 
				 "on p.ID_pi�karza=z.ID_pi�karza, DRU�YNY d "+
				 "where p.ID_dru�yny = d.ID_dru�yny and p.pozycja = 'Bramkarz'"+
				 "group by p.Nazwisko, p.Imie,p.Data_ur,p.Pozycja , d.Nazwa_klubu "+
				 "order by 'Stracone Bramki' desc ");
		 resultSetTableModel.setResultSetTable( resultSet);
		golKipperView.table.setModel(resultSetTableModel);
		
		
			StatusBarControler.SetStatusBarSatte("Wy�wietlenie danych pobranych z bazy");
		
		 
	 }
	 public void FiltersSet(){
		 String FirstName = " '"+(String)golKipperView.comboBox.getSelectedItem()+"' ";
		 String Name = " '"+(String) golKipperView.comboBox_1.getSelectedItem()+"' ";
		 String TeamName = " '"+(String) golKipperView.comboBox_3.getSelectedItem()+"' ";
		 
		 
		 String Birth = new String("");
		 String Loose = new String("");
		 if (FirstName.equals(" ' ' "))
			 FirstName="";
		 else
			 FirstName = " AND p.Nazwisko like "+FirstName;
		 if (Name.equals(" ' ' "))
			 Name="";
		 else
			 Name= " AND p.imie like "+Name;
		 if (TeamName.equals(" ' ' "))
			 TeamName = "";
		 else
			 TeamName = " AND d.Nazwa_klubu like "+TeamName;
		 if (BirthDate.equals(" ' ' "))
			 Birth = "";
		 else {
			 
			 Birth =" AND CONVERT(date,p.Data_ur,120) like "+BirthDate;
			// System.out.println(Birth);
			 BirthDate = " ' ' ";
		}
		 if (LooseGols.equals(" ' ' ")){
			 Loose = "";
		 }
		 else {Loose = " AND strata like "+LooseGols;
		 LooseGols = " ' ' ";}
		 
		 
		 String Query ="where p.ID_dru�yny = d.ID_dru�yny and p.pozycja = 'Bramkarz' "+FirstName+Name+TeamName+Birth+Loose;
		
		 resultSet = DataBaseQuery.Select(ReadOnlyStatment,  "Select p.nazwisko as 'Nazwisko',p.imie as Imi�, CONVERT(date,p.Data_ur,120) as strata, "+
				 "p.Pozycja as 'Pozycja', d.Nazwa_klubu as Klub, sum(z.Strata) as 'Stracone Bramki', "+
				 "sum(z.Czerwona_kartka) as 'Czerwone kartki', sum(z.��ta_kartka) as '��te kartki' "+
				 "from PI�KARZE p "+
				 "left join ZDARZENIA z "+ 
				 "on p.ID_pi�karza=z.ID_pi�karza, DRU�YNY d "+
				 Query+
				 "group by p.Nazwisko, p.Imie,p.Data_ur,p.Pozycja , d.Nazwa_klubu "+
				 "order by 'Stracone Bramki' desc ");
		 resultSetTableModel.setResultSetTable(resultSet);
		 golKipperView.table.setModel(resultSetTableModel);
		 golKipperView.repaint();
		 StatusBarControler.SetStatusBarSatte("Wy�wietlenie odfiltrowanych danych");
	 }
	 
	 public void GetValueForComobox(){
			
			resultSet = DataBaseQuery.Select(ReadWriteStatment, "Select p.nazwisko as 'Nazwisko',p.imie as 'Imi�', "+
				 " d.Nazwa_klubu as Klub "+
				 "from PI�KARZE p "+
				 "left join ZDARZENIA z "+ 
				 "on p.ID_pi�karza=z.ID_pi�karza, DRU�YNY d "+
				 "where p.ID_dru�yny = d.ID_dru�yny and p.pozycja = 'Bramkarz'"+
				 "group by p.Nazwisko, p.Imie,p.Data_ur,p.Pozycja , d.Nazwa_klubu ");
			try {
				
				GkFirstName.add(" ");
				GkName.add(" ");
				GkTeamName.add(" ");
				while (resultSet.next() ){
				GkFirstName.add(resultSet.getString("Nazwisko"));
				GkName.add(resultSet.getString("Imi�"));
				GkTeamName.add(resultSet.getString("Klub"));
				
				}
			} 
				catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				StatusBarControler.SetStatusBarSatte("Ustawienie danych w listach");
			}
			
			 for (String s : GkFirstName )
					golKipperView.comboBox.addItem(s); 
			 for (String s : GkName )
					golKipperView.comboBox_1.addItem(s); 
			 for (String s : GkTeamName )
					golKipperView.comboBox_3.addItem(s); 
			 
	 }
	 
	 public void Events(){
		 golKipperView.btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				FiltersSet();
				
			}
		});
		golKipperView.calendarCombo.addDateListener(new DateListener() {
			
			@Override
			public void dateChanged(DateEvent arg0) {
				 BirthDate = "'"+(String) golKipperView.calendarCombo.getSelectedItem()+"'";
			}
		})	;
		golKipperView.spinner.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent arg0) {
				
				 LooseGols = " '"+(String) golKipperView.spinner.getValue().toString()+"' ";
			}
		});
	 }
	 
	
	public GolKipperView getGolKipperView() {
		return golKipperView;
	}
	public void setGolKipperView(GolKipperView golKipperView) {
		this.golKipperView = golKipperView;
	}
			
}
