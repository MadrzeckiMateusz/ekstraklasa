package program.controler.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.freixas.jcalendar.DateEvent;
import org.freixas.jcalendar.DateListener;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.model.tables.ResultSetTableModel;
import program.view.panels.TimeTableView;

public class TimeTableControler {
	private DataBaseConnector conector;
	 private int ConnectorId;
	 private Statement ReadOnlyStatment;
	 private Statement  ReadWriteStatment;
	 private ResultSet resultSet;
	 private ResultSetTableModel resultSetTableModel;
	 private ArrayList<String> TtTeamNames;
	 private TimeTableView TtView;
	 private String FiltersDatefirst;
	 private String FiltersDateSecond;
	 private String FiltersRoundNumber;
	 
	 public TimeTableControler(){
		 ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
			conector = DataBaseConectorMenager.GetConnection(ConnectorId);
			ReadOnlyStatment = conector.ReadOnlyConection();
			ReadWriteStatment = conector.ReadWriteConection();
			TtTeamNames = new ArrayList<String>();
			resultSetTableModel = new ResultSetTableModel();
			TtView = new TimeTableView();
			FiltersDatefirst = " ' ' ";
			FiltersDateSecond = " ' ' ";
			FiltersRoundNumber = " ' ' ";
			SelectAll();
			SetComboboxValues();
			Events();
		 
	 }
	 
	 public void SelectAll(){
		 DataBaseQuery.DbUpdateViews(ReadWriteStatment, ReadOnlyStatment);
			
			resultSet = DataBaseQuery.Select(ReadOnlyStatment, 
					"SELECT g1.Gospodarz as Gospodarz, g2.Go�� as Go�� , g1.Data_Spotkania as 'Data Spotkania', g1.Kolejka "+
					"FROM gospodarz_view g1 "+
					"LEFT JOIN go��_view g2 "+
					"ON g1.mecze = g2.mecz "
					+ "order by g1.kolejka;");
			
			resultSetTableModel.setResultSetTable(resultSet);
			TtView.table.setModel(resultSetTableModel);
			TtView.repaint();
			
	 }
	 
	 public void Filters(){
		 String teamName= (String)" '"+TtView.comboBox.getSelectedItem()+"' ";
		 
		 
		 String datefirst = new String("");
		 
		 String roundNumber = new String("");
		 String Query = new String ("");
		// System.out.println("fil");
		 
		//	 System.out.println("xxxx");
			 if (teamName.equals(" ' ' "))
			 teamName="";
		 else
			 teamName = " AND g1.Gospodarz like "+teamName+" OR g2.Go�� like "+teamName;
			 
		 if (FiltersDatefirst.equals(" ' ' ") && FiltersDateSecond.equals(" ' ' ") )
			 datefirst="";
		 else{
			 datefirst= " AND (g1.Data_Spotkania > "+FiltersDatefirst+ " AND g1.Data_Spotkania < "+FiltersDateSecond+" ) " ;
			 FiltersDatefirst = " ' ' ";
			 FiltersDateSecond = " ' ' ";
		 }
		 if (FiltersRoundNumber.equals(" ' ' "))
			 roundNumber = "";
		 else{
			 roundNumber = " AND g1.Kolejka like "+FiltersRoundNumber;
			 FiltersRoundNumber = " ' ' ";
		 }
		 
		  Query =teamName+datefirst+roundNumber;
		 
		 
			// System.out.println("query");
		 resultSet = DataBaseQuery.Select(ReadOnlyStatment, 
					"SELECT g1.Gospodarz as Gospodarz, g2.Go�� as Go�� , g1.Data_Spotkania as 'Data Spotkania', g1.Kolejka "+
					"FROM gospodarz_view g1 "+
					"LEFT JOIN go��_view g2 "+
					"ON g1.mecze = g2.mecz "
					+"WHERE  g1.Gospodarz <>'' "+Query
					+ " order by g1.kolejka;");
			
			resultSetTableModel.setResultSetTable(resultSet);
			TtView.table.setModel(resultSetTableModel);
			TtView.repaint();
		 
	 }

	 public void SetComboboxValues(){
		 resultSet = DataBaseQuery.Select(ReadWriteStatment, "Select Nazwa_klubu from dru�yny");
				
		 try {
				
				TtTeamNames.add(" ");
				while (resultSet.next() ){
				TtTeamNames.add(resultSet.getString(1));
			}
		 } 
			catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		 for (String s : TtTeamNames )
				TtView.comboBox.addItem(s); 
				
	 }
	 public void Events(){
		 TtView.calendarCombo.addDateListener(new DateListener() {
			
			@Override
			public void dateChanged(DateEvent arg0) {
				FiltersDatefirst = (String)" '"+TtView.calendarCombo.getSelectedItem()+"' ";
				
			}
		});
		 
		 TtView.calendarCombo_1.addDateListener(new DateListener() {
			
			@Override
			public void dateChanged(DateEvent arg0) {
				FiltersDateSecond = (String)" '"+TtView.calendarCombo_1.getSelectedItem()+"' ";
				
			}
		});
		 
		 TtView.spinner.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent arg0) {
				FiltersRoundNumber = " '"+TtView.spinner.getValue().toString()+"' ";
				
			}
		});
		 TtView.btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Filters();
				
			}
		});
	 }
	public TimeTableView getTtView() {
		return TtView;
	}

	public void setTtView(TimeTableView ttView) {
		TtView = ttView;
	}

}
