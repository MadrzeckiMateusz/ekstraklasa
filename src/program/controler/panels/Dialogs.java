package program.controler.panels;

import java.awt.Component;

import javax.swing.JOptionPane;

public class Dialogs {

	
	public static void ShowErrorDialog(Component frame, String text, String title){
		JOptionPane.showMessageDialog(frame,
			    text,
			    title,
			    JOptionPane.ERROR_MESSAGE);
	}
	public static int ShowInfoDialog(Component frame, String text, String title){
		/*JOptionPane.showMessageDialog(frame,
			    text,
			    title,
			    JOptionPane.INFORMATION_MESSAGE);*/
		int a = JOptionPane.showConfirmDialog(frame, text,
				title,JOptionPane.YES_OPTION);
				
		return a;
	}
}
