package program.controler.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.view.panels.AddDataView;
import program.view.panels.DataBaseManagmentView;
import program.view.panels.UpdateDataView;

public class UpdateDateControler {
	
	private DataBaseConnector conector;
	 private int ConnectorId;
	 private Statement  ReadWriteStatment;
	private ResultSet resultSet;
	private UpdateDataView udView;
	private DataBaseManagmentView dbmView;
	private String TableName;
	private ResultSetMetaData resultSetMetaData;
	private ArrayList<String> ColumNames;
	private ArrayList<String> ColumnTypes;
	private ArrayList<JLabel> ColunLabels;
	private ArrayList<JFormattedTextField> textFields;
	private int ColumnCount;
	private int row;
	
	public UpdateDateControler(ResultSet resultSet , String TableName,DataBaseManagmentView dbmview){
		ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
		conector = DataBaseConectorMenager.GetConnection(ConnectorId);
		ReadWriteStatment = conector.ReadWriteConection();
		this.resultSet = resultSet;
		this.TableName = TableName;
		this.udView = new UpdateDataView();
		this.dbmView = dbmview;
		row= dbmView.table.getSelectedRow();
		this.ColumNames = new ArrayList<String>();
		this.ColumnTypes = new ArrayList<String>();
		this.ColunLabels = new ArrayList<JLabel>();
		this.textFields = new ArrayList<JFormattedTextField>();
		try {
			this.resultSetMetaData = this.resultSet.getMetaData();
			ColumnCount = resultSetMetaData.getColumnCount();
		} catch (SQLException e) {
			String Error = e.getMessage();
			Dialogs.ShowErrorDialog(udView, Error , "B��d");
		}
		
		SetComponents();
		
		Event();
	}
	public void UpdateData(){
		
		String Values="";
		for (int i=0;i<textFields.size();i++){
			if(i==textFields.size()-1)
			Values+=" "+ColunLabels.get(i).getText()+"='"+textFields.get(i).getText().trim()+"'";
			else
			Values+=" "+ColunLabels.get(i).getText()+"='"+textFields.get(i).getText().trim()+"', ";
		}
		if(TableName.equals("Gospodarz")|| TableName.equals("Go��")||TableName.equals("S�dzia_meczu"))
		{
			try {
				DataBaseQuery.UpdateQuery(ReadWriteStatment, "Update "+TableName
						+ " Set "+Values+" Where "+ColunLabels.get(0).getText()+" = '"+textFields.get(0).getText().trim()+"' AND "
								+ ColunLabels.get(1).getText()+" = '"+textFields.get(1).getText().trim()+"'");
			} catch (SQLException e) {
				String Error = e.getMessage();
				Dialogs.ShowErrorDialog(udView, Error , "B��d");
			}finally{
				StatusBarControler.SetStatusBarSatte("Zaktualizowano rekord "+row);
			}
		}else{
			
			try {
				DataBaseQuery.UpdateQuery(ReadWriteStatment, "Update "+TableName
					+ " Set "+Values+" Where "+ColunLabels.get(0).getText()+" = '"+textFields.get(0).getText().trim()+"'");
			} catch (SQLException e) {
				String Error = e.getMessage();
				Dialogs.ShowErrorDialog(udView, Error , "B��d");
			}finally{
				StatusBarControler.SetStatusBarSatte("Zaktualizowano rekord "+row);
				int a =Dialogs.ShowInfoDialog(udView, "Rekord zaktualizowany", "Aktualizacja rekordu");
				if (a==0){
					udView.dispose();
				}
			}
		}
		
	 }
	public void SetComponents(){
		try {
			
			 //# - Any valid number, uses Character.isDigit.
            //' - Escape character, used to escape any of the special formatting characters.
            //U - Any character (Character.isLetter). All lowercase letters are mapped to upper case.
            //L - Any character (Character.isLetter). All upper case letters are mapped to lower case.
            //A - Any character or number (Character.isLetter or Character.isDigit)
            //? - Any character (Character.isLetter).
            //* - Anything.
            //H - Any hex character (0-9, a-f or A-F).
			int positions_Y = 5;
			
			for(int i=0; i<ColumnCount;i++){
			 ColumNames.add(resultSetMetaData.getColumnName(i+1));
			 ColumnTypes.add(resultSetMetaData.getColumnTypeName(i+1));
			// System.out.println(ColumnTypes.get(i));
			 ColunLabels.add(MakeJLabel(ColumNames.get(i), positions_Y));
			 udView.panel.add(ColunLabels.get(i));
			 String format = "";
			 if(ColumnTypes.get(i).equals("int"))
				 format = "##########";
			 if(ColumnTypes.get(i).equals("nvarchar"))
				 format = "AAAAAAAAAAAAAAAAAAA";
			 if(ColumnTypes.get(i).equals("datetime"))
				 format = "####-##-##";
			 if(ColumnTypes.get(i).equals("varchar"))
			 {
				 int Lenght = resultSetMetaData.getColumnDisplaySize(i+1);
				 while(Lenght!=0){
					 format +="?";
					 Lenght--;
				 }
				 
			 }
			 textFields.add(makeJtexJTextField(positions_Y,format));
			 udView.panel.add(textFields.get(i));
			 positions_Y +=25;
			 
			}
			
			for (int j =0;j<ColumnCount;j++){
				textFields.get(j).setText(dbmView.table.getModel().getValueAt(row, j).toString());
			}
			if(TableName.equals("Gospodarz")|| TableName.equals("Go��")||TableName.equals("S�dzia_meczu"))
			{
				textFields.get(0).setEditable(false);
				textFields.get(1).setEditable(false);
			}else{
				
				textFields.get(0).setEditable(false);
			}
		} catch (SQLException e) {
			String Error = e.getMessage();
			Dialogs.ShowErrorDialog(udView, Error , "B��d");
		}finally{
			StatusBarControler.SetStatusBarSatte("Pobrano dane z wiersza "+row);
		}
		
		
	}
	
	
	public MaskFormatter createFormatter(String s) {
	    MaskFormatter formatter = null;
	    try {
	        formatter = new MaskFormatter(s);
	        formatter.setPlaceholderCharacter(' ');
	       
	    } catch (java.text.ParseException exc) {
	  //      System.err.println("formatter is bad: " + exc.getMessage());
	     //   System.exit(-1);
	    }
	    return formatter;
	}
	
	public JLabel MakeJLabel(String Name, int Position_Y){
		JLabel label = new JLabel(Name);
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setBounds(52, Position_Y, 120, 20);
		return label;
	}
	
	public JFormattedTextField makeJtexJTextField(int Position_Y, String formater){
		JFormattedTextField textField;
		if (formater.equals(""))
		 textField = new JFormattedTextField();
		else		
		textField = new JFormattedTextField();
		textField.setFormatterFactory(new DefaultFormatterFactory(createFormatter(formater)));
        textField.setFocusLostBehavior(JFormattedTextField.COMMIT);
		textField.setEditable(true);
		textField.setBounds(182, Position_Y, 150, 20);
	
		return textField;
		
	}
	
	public void Event(){
		udView.Update.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UpdateData();
				
			}
		});
		udView.Cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				udView.dispose();
				
			}
		});
	}

}
