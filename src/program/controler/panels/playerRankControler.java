package program.controler.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.model.tables.ResultSetTableModel;
import program.view.panels.playerRankView;


public class playerRankControler {

	private DataBaseConnector conector;
	private int ConnectorId;
	 private Statement ReadOnlyStatment;
	 private ResultSet resultSet;
	 private ResultSetTableModel resultSetTableModel;
	 private playerRankView pRankView;
	 private MoreOfResultControler moreresultking;
	 
	 public  playerRankControler() {
		 ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
		 conector = DataBaseConectorMenager.GetConnection(ConnectorId);
		 ReadOnlyStatment = conector.ReadOnlyConection();
		 resultSetTableModel = new ResultSetTableModel();
		 pRankView = new playerRankView();
		 SelectAll();
		 Events();
		 
	 }
	 
	 public void SelectAll(){
		 resultSet = DataBaseQuery.Select(ReadOnlyStatment, 
				 "Select p.nazwisko as 'Nazwisko',p.imie as Imi�, CONVERT(date,p.Data_ur,120) as 'Data urodzenia', "+
				 "p.Pozycja as 'Pozycja', d.Nazwa_klubu as Klub, sum(z.gol) as 'Bramki', "+
				 "sum(z.Czerwona_kartka) as 'Czerwone kartki', sum(z.��ta_kartka) as '��te kartki' "+
				 "from PI�KARZE p "+
				 "left join ZDARZENIA z "+ 
				 "on p.ID_pi�karza=z.ID_pi�karza, DRU�YNY d "+
				 "where p.ID_dru�yny = d.ID_dru�yny and p.pozycja<>'Bramkarz'"+
				 "group by p.Nazwisko, p.Imie,p.Data_ur,p.Pozycja , d.Nazwa_klubu "+
				 "order by Bramki desc ");
		 resultSetTableModel.setResultSetTable( resultSet);
		 pRankView.table.setModel(resultSetTableModel);
		 StatusBarControler.SetStatusBarSatte("Wy�wietlenie danych pobranych z bazy");
		 
	 }
	 public void SelecKing(){
		 int gol = 0;
		 resultSet = DataBaseQuery.Select(ReadOnlyStatment, "select   MAX( bramka) as bramka from strzelcy");
		 try {
			resultSet.last();
			gol = resultSet.getInt(1);
			System.out.println(gol);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 moreresultking = new MoreOfResultControler();
		 moreresultking.SelectQueryMoreResult("select Nazwisko , Imi� , Pozycja , klub , bramka as gole from strzelcy"+
				 " Where bramka like '"+gol+"'"+
				 
				 "order by bramka desc ");

		 
	 }
	 public void Events(){
		 pRankView.king.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				SelecKing();
				
			}
		});
	 }
	public playerRankView getpRankView() {
		return pRankView;
	}

	public void setpRankView(playerRankView pRankView) {
		this.pRankView = pRankView;
	}
}
