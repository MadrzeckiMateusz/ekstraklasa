package program.controler.panels;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.view.panels.DataBaseManagmentView;

public class RemoveDataControler {
	private DataBaseConnector conector;
	 private int ConnectorId;
	 private Statement  ReadWriteStatment;
	private ResultSet resultSet;
	private DataBaseManagmentView dbmView;
	private String TableName;
	private ResultSetMetaData resultSetMetaData;
	
	public RemoveDataControler(ResultSet resultSet, String TableName, DataBaseManagmentView dbmview){
		ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
		conector = DataBaseConectorMenager.GetConnection(ConnectorId);
		ReadWriteStatment = conector.ReadWriteConection();
		this.resultSet = resultSet;
		this.TableName = TableName;
		this.dbmView = dbmview;
		
		try {
			this.resultSetMetaData = this.resultSet.getMetaData();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RemoveData();
	}
	public void RemoveData(){
		int row= dbmView.table.getSelectedRow();
		
		if(TableName.equals("Gospodarz")|| TableName.equals("Go��")||TableName.equals("S�dzia_meczu"))
		{
			try {
				DataBaseQuery.UpdateQuery(ReadWriteStatment, "delete from "+TableName+" where "+dbmView.table.getModel().getColumnName(0)+"='"+
dbmView.table.getModel().getValueAt(row, 0)+"' AND "+ dbmView.table.getModel().getColumnName(1)+"= '"+dbmView.table.getModel().getValueAt(row, 1)+"'");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				
			}

		}else{
			
			try {
				DataBaseQuery.UpdateQuery(ReadWriteStatment, "delete from "+TableName+" where "+dbmView.table.getModel().getColumnName(0)+"='"+
						dbmView.table.getModel().getValueAt(row, 0)+"'");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
