package program.controler.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.model.tables.ResultSetTableModel;
import program.view.panels.DataBaseManagmentView;

public class DataBaseManagmentControler {
	private DataBaseConnector conector;
	 private int ConnectorId;
	 private Statement ReadOnlyStatment;
	 private Statement  ReadWriteStatment;
	 private ResultSet resultSet;
	 private ResultSet dataBaseMetadaResultSet;
	 private ResultSetTableModel resultSetTableModel;
	 private DataBaseManagmentView DBMView;
	 private DatabaseMetaData metaData;
	 private ResultSetMetaData resultdetMetaData;
	 private String TableName;
	 
	 public DataBaseManagmentControler(){
		 ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
			conector = DataBaseConectorMenager.GetConnection(ConnectorId);
			metaData = conector.DataBaseName();
			ReadOnlyStatment = conector.ReadOnlyConection();
			ReadWriteStatment = conector.ReadWriteConection();
			
			DBMView = new DataBaseManagmentView();
			TableName = "";
			SetTablesName();
			Events();
			
	 }
	 
	 public void SetTablesName(){
		 try { 
				dataBaseMetadaResultSet = metaData.getTables(null,null,null,new String[] {"TABLE"});
				DBMView.comboBox.addItem("");
				while(dataBaseMetadaResultSet.next()){
					DBMView.comboBox.addItem(dataBaseMetadaResultSet.getString(3));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 }
	 public void SelectAll(){
		 resultSetTableModel = new ResultSetTableModel();
		 TableName = DBMView.comboBox.getSelectedItem().toString();
		 resultSet = DataBaseQuery.SelectFrom(ReadOnlyStatment, TableName);
		 resultSetTableModel.setResultSetTable(resultSet);
		  DBMView.table.setModel(resultSetTableModel);
		// resultSetTableModel.SetColumnWidth(DBMView.table);
		 DBMView.table.repaint();
		
		
		 StatusBarControler.SetStatusBarSatte("Wyświetlanie danych z tabeli "+TableName);
			
		 
	 }
	 public void Events(){
		 DBMView.btnNewButton_3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SelectAll();
						
				
			}
		});
		 DBMView.btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new AddDataControler(resultSet, TableName);
				
			}
		});
		 DBMView.btnNewButton_1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new UpdateDateControler(resultSet, TableName, DBMView);
			}
		});
		 
		 DBMView.btnNewButton_2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new RemoveDataControler(resultSet, TableName, DBMView);
				
			}
		});
	 }

	public DataBaseManagmentView getDBMView() {
		return DBMView;
	}

	public void setDBMView(DataBaseManagmentView dBMView) {
		DBMView = dBMView;
	}
}
