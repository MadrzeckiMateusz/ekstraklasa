package program.controler.panels;

import program.view.panels.StatusBarView;

public class StatusBarControler {
	private static StatusBarView StBView;
	
	

	public StatusBarControler(){
		StBView = new StatusBarView();
	}
	public static void SetStatusBarSatte(String text){
		StBView.textField.setText(text);
		StBView.textField.repaint();
		
	}
	public StatusBarView getStBView() {
		return StBView;
	}

	public void setStBView(StatusBarView stBView) {
		StBView = stBView;
	}
}
