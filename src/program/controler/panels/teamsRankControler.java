package program.controler.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.Statement;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.model.tables.ResultSetTableModel;
import program.view.panels.teamsRankView;

public class teamsRankControler {
	
	private DataBaseConnector conector;
	private int ConnectorId;
	 private Statement ReadOnlyStatment;
	 private ResultSet resultSet;
	 private ResultSetTableModel resultSetTableModel;
	 private teamsRankView trView;
	 private MoreOfResultControler moreresult;
	 public teamsRankControler(){
		 ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
		 conector = DataBaseConectorMenager.GetConnection(ConnectorId);
		 ReadOnlyStatment = conector.ReadOnlyConection();
		 resultSetTableModel = new ResultSetTableModel();
		 trView = new teamsRankView();
		 SelectAll();
		 Events();
	 }
	 
	 public void SelectAll(){
		 
		 resultSet = DataBaseQuery.Select(ReadOnlyStatment, 
				 "Select d.Nazwa_klubu as 'Nazwa klubu', d.Barwy as Barwy, d.Pozycja_ligowa as 'Pozycja ligowa', d.Punkty as Punkty, "+
				 "d.Wygrane_mecze + d.Przegrane_mecze + d.Remisowe_mecze as 'Rozegrane mecze', "+
				 "d.ilo��_zdobytych_bramek as 'Bramki zdobyte', d.ilo��_straconych_bramek as 'Bramki stracone' from Dru�yny d "+
				 "order by d.Pozycja_ligowa; ");
		 resultSetTableModel.setResultSetTable( resultSet);
		 trView.table.setModel(resultSetTableModel);
	 }
	 public void SelectAvgAge(){
		 moreresult = new MoreOfResultControler();
		
		moreresult.SelectQueryMoreResult( "select AVG( Year(GETDATE())- YEAR( p.Data_ur)) as 'Wiek �redni' "
		 		+ " ,d.Nazwa_klubu as 'Nazwa klubu' from "+
				 " Pi�karze p , DRU�YNY d "+
				 "Where p.ID_dru�yny = d.ID_dru�yny "+
				 "group by d.Nazwa_klubu;");
	 }
	 
	 public void Events(){
		 trView.avgwiek.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				SelectAvgAge();
				
			}
		});
	 }
	public teamsRankView getTrView() {
		return trView;
	}

	public void setTrView(teamsRankView trView) {
		this.trView = trView;
	}

}
