package program.controler.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.freixas.jcalendar.DateEvent;
import org.freixas.jcalendar.DateListener;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.model.tables.ResultSetTableModel;
import program.view.panels.ReportsView;

public class ReportsControler {
	
	 private DataBaseConnector conector;
	 private int ConnectorId;
	 private Statement ReadOnlyStatment;
	 private Statement  ReadWriteStatment;
	 private ResultSet resultSet;
	 private ResultSetTableModel resultSetTableModel;
	 private MoreOfResultControler moreOfResultControler;
	 private ReportsView rpView;
	 
	 private String DateBirthArbiter;
	 private String ArbiterNumber;
	 	 
	 public ReportsControler(){
		 ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
			conector = DataBaseConectorMenager.GetConnection(ConnectorId);
			ReadOnlyStatment = conector.ReadOnlyConection();
			ReadWriteStatment = conector.ReadWriteConection();
			resultSetTableModel = new ResultSetTableModel();
			rpView = new ReportsView();
			DateBirthArbiter = " ' ' ";
			ArbiterNumber = " ' ' ";
			SelectAll();
			SetComboboxValues();
			Filters();
			Events();
	 }
	 
	 public void Filters(){
		 String FirstName= " '"+rpView.comboBox.getSelectedItem().toString()+"' ";
		 String Name = " '"+rpView.comboBox_1.getSelectedItem().toString()+"' ";
		 
		 String Datebirth = new String("");
		 
		 String ArbiterNumber = new String("");
		 String Query = new String ("");
		 
		 
			 
			if (FirstName.equals(" ' ' "))
			 FirstName="";
		 else
			 FirstName = " AND s.nazwisko like "+FirstName;
			 
		 if (Name.equals(" ' ' ") )
			 Name="";
		 else{
			 Name= " AND s.imie "+Name;
			 
		 }
		 if (DateBirthArbiter.equals(" ' ' "))
			 Datebirth = "";
		 else{
			Datebirth = " AND 'data urodzenia' like "+DateBirthArbiter;
			 DateBirthArbiter = " ' ' ";
		 }
		 if(this.ArbiterNumber.equals(" ' ' ")){
			 ArbiterNumber ="";
		 }else {
			 ArbiterNumber = " AND s.ID_sedziego like "+ this.ArbiterNumber;
			 this.ArbiterNumber =" ' ' ";
		 }
		 
		  Query =FirstName + Name + Datebirth +ArbiterNumber;
		 
		 
		  resultSet = DataBaseQuery.Select(ReadOnlyStatment, 
					"Select s.ID_sedziego ,s.imie, s.nazwisko, convert(date,s.data_ur,120) as 'data urodzenia', "+
			" count(sm.id_meczu) as 'Ilo�� meczy' from S�DZIOWIE s left join s�dzia_meczu sm on s.ID_sedziego = sm.ID_sedziego "
			+ " Where s.ID_sedziego <> '' "+Query+ 
					" group by s.imie,s.nazwisko,s.ID_sedziego,s.Data_ur");
			
			resultSetTableModel.setResultSetTable(resultSet);
			rpView.table.setModel(resultSetTableModel);
			rpView.repaint();
		 
	 }

	 
	 public void SelectAll(){
		 DataBaseQuery.DbUpdateViews(ReadWriteStatment, ReadOnlyStatment);
			
			resultSet = DataBaseQuery.Select(ReadOnlyStatment, 
					"Select s.ID_sedziego ,s.imie, s.nazwisko, convert(date,s.data_ur,120) as 'data urodzenia', "+
			" count(sm.id_meczu) as 'Ilo�� meczy' from S�DZIOWIE s left join s�dzia_meczu sm on s.ID_sedziego = sm.ID_sedziego "+
			" Where s.ID_sedziego <> '' "+
					" group by s.imie,s.nazwisko,s.ID_sedziego,s.Data_ur");
			
			resultSetTableModel.setResultSetTable(resultSet);
			rpView.table.setModel(resultSetTableModel);
			rpView.repaint();
	 }
	 
	 public void GetSelectedRow(){
		int row= rpView.table.getSelectedRow();
		int ArbiterNum= (int) rpView.table.getModel().getValueAt(row, 0);
		moreOfResultControler = new MoreOfResultControler();
		moreOfResultControler.SelectQueryMoreResult(""
				+ "Select z.ID_zdarzenia as'Numer Zdarzenia', z.ID_meczu as 'Numer Spotkania',"+
				" z.ID_pi�karza as 'Numer Pi�karza', z.Czas_zdarzenia, z.Gol , z.Strata,"+
				" z.Czerwona_kartka as 'Czerwona kartka', z.��ta_kartka as '��ta kartka'"+
				" from ZDARZENIA z ,mecze m , S�dzia_meczu sm"+ 
				" where sm.ID_sedziego = "+"'"+ArbiterNum+"' and sm.ID_meczu = z.ID_meczu"+
				" group by z.ID_meczu,z.ID_zdarzenia,z.ID_pi�karza,z.Czas_zdarzenia,z.Gol,z.Strata,z.Czerwona_kartka,z.��ta_kartka");
		
	 }
	 
	 public void SetComboboxValues(){
		 resultSet = DataBaseQuery.Select(ReadWriteStatment, "Select Nazwisko , Imie from S�dziowie");
				
		 try {
				
			 rpView.comboBox.addItem(" ");
			 rpView.comboBox_1.addItem(" ");
			 while (resultSet.next() ){
				rpView.comboBox.addItem(resultSet.getString(1));
				rpView.comboBox_1.addItem(resultSet.getString(2));
			}
		 } 
			catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
				
	 }
	 public void Events(){
		rpView.btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GetSelectedRow();
				
			}
		});
		
		rpView.calendarCombo.addDateListener(new DateListener() {
			
			@Override
			public void dateChanged(DateEvent arg0) {
				DateBirthArbiter = " '"+rpView.calendarCombo.getSelectedItem().toString()+"' ";
				
			}
		});
		
		rpView.spinner.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent arg0) {
				ArbiterNumber= " '"+rpView.spinner.getValue().toString()+"' ";
				
			}
		});
		rpView.btnNewButton_1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Filters();
				
			}
		});
	 }

	public ReportsView getRpView() {
		return rpView;
	}

	public void setRpView(ReportsView rpView) {
		this.rpView = rpView;
	}

}
