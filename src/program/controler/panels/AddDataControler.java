package program.controler.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.view.panels.AddDataView;

public class AddDataControler {
	private DataBaseConnector conector;
	 private int ConnectorId;
	 private Statement ReadOnlyStatment;
	 private Statement  ReadWriteStatment;
	private ResultSet resultSet;
	private AddDataView adView;
	private String TableName;
	private ResultSetMetaData resultSetMetaData;
	private ArrayList<String> ColumNames;
	private ArrayList<String> ColumnTypes;
	private ArrayList<JLabel> ColunLabels;
	private ArrayList<JFormattedTextField> textFields;
	
	public AddDataControler(ResultSet resultSet , String TableName){
		 ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
			conector = DataBaseConectorMenager.GetConnection(ConnectorId);
			ReadOnlyStatment = conector.ReadOnlyConection();
			ReadWriteStatment = conector.ReadWriteConection();
		this.resultSet = resultSet;
		this.TableName = TableName;
		this.adView = new AddDataView();
		this.ColumNames = new ArrayList<String>();
		this.ColumnTypes = new ArrayList<String>();
		this.ColunLabels = new ArrayList<JLabel>();
		this.textFields = new ArrayList<JFormattedTextField>();
		try {
			this.resultSetMetaData = this.resultSet.getMetaData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SetComponents();
		SelectLastId();
		Event();
	}
	
	public void AddDataToDataBase(){
		String Values="";
		for (int i=0;i<textFields.size();i++){
			if(i==textFields.size()-1)
			Values+=" '"+textFields.get(i).getText().trim()+"'";
			else
			Values+=" '"+textFields.get(i).getText().trim()+"', ";
		}
	//	System.out.println(Values);
		try {
			DataBaseQuery.UpdateQuery(ReadWriteStatment, "Insert into "+TableName
					+ " Values( "+Values+" )");
		} catch (SQLException e) {
			String Error = e.getMessage();
			Dialogs.ShowErrorDialog(adView, Error , "B��d");
		}finally{
			int a =Dialogs.ShowInfoDialog(adView, "Rekord dodany prawid�owo", "Dodanie rekordu");
		if (a==0){
			adView.dispose();
		}
		}
		DataBaseQuery.DbUpdateViews(ReadWriteStatment, ReadOnlyStatment);
		
	}
	public void SetComponents(){
		try {
			
			 //# - Any valid number, uses Character.isDigit.
            //' - Escape character, used to escape any of the special formatting characters.
            //U - Any character (Character.isLetter). All lowercase letters are mapped to upper case.
            //L - Any character (Character.isLetter). All upper case letters are mapped to lower case.
            //A - Any character or number (Character.isLetter or Character.isDigit)
            //? - Any character (Character.isLetter).
            //* - Anything.
            //H - Any hex character (0-9, a-f or A-F).
			int positions_Y = 5;
			int ColumnCount = resultSetMetaData.getColumnCount();
			for(int i=0; i<ColumnCount;i++){
			 ColumNames.add(resultSetMetaData.getColumnName(i+1));
			 ColumnTypes.add(resultSetMetaData.getColumnTypeName(i+1));
			// System.out.println(ColumnTypes.get(i));
			 ColunLabels.add(MakeJLabel(ColumNames.get(i), positions_Y));
			 adView.panel.add(ColunLabels.get(i));
			 String format = "";
			 if(ColumnTypes.get(i).equals("int"))
				 format = "##########";
			 if(ColumnTypes.get(i).equals("nvarchar"))
				 format = "AAAAAAAAAAAAAAAAAAA";
			 if(ColumnTypes.get(i).equals("datetime"))
				 format = "####-##-##";
			 if(ColumnTypes.get(i).equals("varchar"))
			 {
				 int Lenght = resultSetMetaData.getColumnDisplaySize(i+1);
				 while(Lenght!=0){
					 format +="?";
					 Lenght--;
				 }
				 
			 }
			 textFields.add(makeJtexJTextField(positions_Y,format));
			 adView.panel.add(textFields.get(i));
			 positions_Y +=25;
			}
		} catch (SQLException e) {
			String Error = e.getMessage();
			Dialogs.ShowErrorDialog(adView, Error , "B��d");
		}
		
	}
	public void SelectLastId(){
		resultSet = DataBaseQuery.SelectFrom(ReadOnlyStatment, TableName);
		try {
			resultSet.last();
			
			int value = resultSet.getInt(1) + 1;
			textFields.get(0).setText(String.valueOf(value ));
		} catch (SQLException e) {
			String Error = e.getMessage();
			Dialogs.ShowErrorDialog(adView, Error , "B��d");
		}
				
	}
	public MaskFormatter createFormatter(String s) {
	    MaskFormatter formatter = null;
	    try {
	        formatter = new MaskFormatter(s);
	        formatter.setPlaceholderCharacter(' ');
	       
	    } catch (java.text.ParseException exc) {
	    	
	      //  System.err.println("formatter is bad: " + exc.getMessage());
	       //System.exit(-1);
	    }
	    return formatter;
	}
	
	public JLabel MakeJLabel(String Name, int Position_Y){
		JLabel label = new JLabel(Name);
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setBounds(52, Position_Y, 120, 20);
		return label;
	}
	
	public JFormattedTextField makeJtexJTextField(int Position_Y, String formater){
		JFormattedTextField textField;
		if (formater.equals(""))
		 textField = new JFormattedTextField();
		else		
		textField = new JFormattedTextField();
		textField.setFormatterFactory(new DefaultFormatterFactory(createFormatter(formater)));
        textField.setFocusLostBehavior(JFormattedTextField.COMMIT);
		textField.setEditable(true);
		textField.setBounds(182, Position_Y, 150, 20);
	
		return textField;
		
	}
	
	public void Event(){
		adView.add.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AddDataToDataBase();
				
			}
		});
		adView.Cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				adView.dispose();
				
			}
		});
	}


}


