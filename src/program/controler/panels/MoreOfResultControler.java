package program.controler.panels;

import java.sql.ResultSet;
import java.sql.Statement;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.model.tables.ResultSetTableModel;
import program.view.panels.ViewMoreOfResult;

public class MoreOfResultControler {
	
	 private DataBaseConnector conector;
	 private int ConnectorId;
	 private Statement ReadOnlyStatment;
	 private Statement  ReadWriteStatment;
	 private ResultSet resultSet;
	 private ResultSetTableModel resultSetTableModel;
	 private ViewMoreOfResult viewMoreOfResult;
	
	public MoreOfResultControler(){
		 ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
			conector = DataBaseConectorMenager.GetConnection(ConnectorId);
			ReadOnlyStatment = conector.ReadOnlyConection();
			ReadWriteStatment = conector.ReadWriteConection();
			resultSetTableModel = new ResultSetTableModel();
		viewMoreOfResult = new ViewMoreOfResult();
	}
	public void SelectQueryMoreResult(String Query){
		resultSet =  DataBaseQuery.Select(ReadOnlyStatment,Query);
		resultSetTableModel.setResultSetTable(resultSet);
		viewMoreOfResult.table.setModel(resultSetTableModel);
		viewMoreOfResult.setVisible(true);
	}
	
	
	
}
