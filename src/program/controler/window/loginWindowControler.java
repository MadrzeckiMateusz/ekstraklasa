package program.controler.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import program.database.DataBaseConectorMenager;
import program.database.DataBaseConnector;
import program.database.DataBaseQuery;
import program.view.window.LoginWindow;
import program.view.window.mainWindow;

public class loginWindowControler {

	private DataBaseConnector conector;
	private int ConnectorId;
	
	private Statement  ReadWriteStatment;
	private Statement  ReadOnlyStatment;
	private ResultSet resultSet;
	
	private static LoginWindow loginWindow;
	private mainWindowControler MainWindowCntroler;
	private mainWindow MainWindow;
	private ArrayList<String> UsersName;
	
	
	 
	 public loginWindowControler(){
		
		ConnectorId = DataBaseConectorMenager.AddConector(getClass().getName());
		conector = DataBaseConectorMenager.GetConnection(ConnectorId);
		 ReadOnlyStatment = conector.ReadOnlyConection();
		 ReadWriteStatment = conector.ReadWriteConection();
		 loginWindow = new LoginWindow();
		 MainWindowCntroler = new mainWindowControler();
		 MainWindow = MainWindowCntroler.getMainWindow();
		 UsersName = new ArrayList<String>();
		
		 
		 loginWindow.setVisible(true);
		 
		 SetUserData();
		 Events();
		  
	 }
	 
	 public void SetUserData(){
		 
		 resultSet = DataBaseQuery.SelectFrom(ReadWriteStatment, "użytkownicy");
		 try {
				UsersName.add(" ");
				while (resultSet.next() ){
				UsersName.add(resultSet.getString(1));
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}		
			for (String s : UsersName)
			loginWindow.comboBox.addItem(s);
			
			
		 
	 }
	public boolean CheckPassword(String user,String insertedpassword){
		String password = null;
		String paswordok = new String(insertedpassword);
		String Query = "Select haslo from użytkownicy "+
				 "where nazwa_użytkownika = '"+user+"'";
		 resultSet = DataBaseQuery.Select(ReadOnlyStatment,Query );
		 try {
			 while(resultSet.next()){
			password = new String( resultSet.getString(1));
			}
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 if (password.equals(paswordok))
			 return true;
		 else
			 return false;
		 
	 }
	public String CheckUserStatus(String user){
		String status = null;
		String Query = "Select Status from użytkownicy "+
				 "where nazwa_użytkownika = '"+user+"'";
		 resultSet = DataBaseQuery.Select(ReadOnlyStatment,Query );
		 try {
			 while(resultSet.next()){
			status= new String( resultSet.getString(1));
			}
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return status;
	}
	public static void Setvisiblelogin(Boolean b){
		loginWindow.setVisible(b);
	}
	 
	 public void Events(){
		 loginWindow.btnNewButton_1.addActionListener(new ActionListener() {
			 
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String user = (String) loginWindow.comboBox.getSelectedItem();
				if(CheckPassword(user, loginWindow.passwordField.getText()))
				{	String status= CheckUserStatus(user);
					if (status.equals(new String("kibic")))
					{	//System.out.println("działa");
						MainWindow.horizontalTabs.setEnabledAt(3,false);
						MainWindow.horizontalTabs.setEnabledAt(4,false);
					}
					if (status.equals(new String("pzpn"))){
						MainWindow.horizontalTabs.setEnabledAt(3, true);
						MainWindow.horizontalTabs.setEnabledAt(4, false);
					}
					if (status.equals(new String("admin"))){
						MainWindow.horizontalTabs.setEnabledAt(3, true);
						MainWindow.horizontalTabs.setEnabledAt(4, true);
					}
				
				MainWindow.setVisible(true);
				loginWindow.setVisible(false);
				loginWindow.comboBox.setSelectedIndex(0);
				loginWindow.passwordField.setText(null);
				}
				
			}
		});
		 loginWindow.btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				DataBaseConectorMenager.CloseAllConection();
				loginWindow.CloseWindow();
				
			}
		});
	 }
	 
	
}
