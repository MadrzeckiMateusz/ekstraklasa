package program.controler.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JOptionPane;

import program.database.DataBaseConectorMenager;
import program.view.window.mainWindow;

public class mainWindowControler {
	
	private mainWindow MainWindow;
	
	
	public mainWindowControler(){
		MainWindow = new mainWindow();
	//	MainWindow.setVisible(true);
		Events();
	}
	public void Events(){
		MainWindow.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosing(WindowEvent arg0) {
				DataBaseConectorMenager.CloseAllConection();
				CloseWindow();
				
			}
			
			@Override
			public void windowClosed(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowActivated(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		MainWindow.Close.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				CloseWindow();
				
			}
		});
		MainWindow.logout.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				loginWindowControler.Setvisiblelogin(true);
				MainWindow.setVisible(false);
				
			}
		});
	}
	public void CloseWindow()
	{
		int a = JOptionPane.showConfirmDialog(MainWindow, "Czy zako�czy� program ?",
				"Ko�czenie pracy programu",JOptionPane.OK_CANCEL_OPTION );
				if (a == JOptionPane.OK_OPTION )
		{
		
			System.exit(0);
		}
		else
			{
				return;
			}
	}


	public mainWindow getMainWindow() {
		return MainWindow;
	}


	public void setMainWindow(mainWindow mainWindow) {
		MainWindow = mainWindow;
	}

}
