package program.model.tables;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class ResultSetTableModel extends AbstractTableModel {
	private ResultSet rs;
	private ResultSetMetaData rsmd;
	public ResultSetTableModel(){
		
	}
	public void setResultSetTable(ResultSet aResultSet){
		rs = aResultSet;
		
		try {
			rsmd = rs.getMetaData();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}

	public String getColumnName(int column){
		try {
			return rsmd.getColumnName(column+1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	
	public int getColumnCount() {
		try {
			return rsmd.getColumnCount();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
		
	}

	@Override
	public int getRowCount() {
		
		try {
			rs.last();
			return rs.getRow();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
		
	}

	@Override
	public Object getValueAt(int row, int column) {
		
		
		try {
			rs.absolute(row + 1);
			return rs.getObject(column +1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	public void SetColumnWidth(JTable table){
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		TableColumnModel columnModel = table.getColumnModel();
		
		try {
			int columnCount = columnModel.getColumnCount();
			for (int i=1;i<columnCount;i++){
				int widthcolumn = rsmd.getColumnName(i).length();
				System.out.println(rsmd.getColumnName(i));
				TableColumn column = columnModel.getColumn(i);
				column.setPreferredWidth(widthcolumn);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
