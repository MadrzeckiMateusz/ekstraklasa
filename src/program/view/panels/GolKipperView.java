package program.view.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.DateFormat;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import org.freixas.jcalendar.JCalendarCombo;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.ImageIcon;

public class GolKipperView extends JPanel {
	public JTable table;
	public JScrollPane scrollPane;
	public JPanel panel;
	public JLabel lblNewLabel;
	public JComboBox comboBox;
	public JLabel lblNewLabel_1;
	public JComboBox comboBox_1;
	public JLabel lblNewLabel_2;
	public JLabel lblNewLabel_3;
	public JLabel lblNewLabel_4;
	public JComboBox comboBox_3;
	public JButton btnNewButton;
	public JSpinner spinner;
	public JCalendarCombo calendarCombo;
	public GolKipperView() {
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setMinimumSize(new Dimension(900, 450));
		setPreferredSize(new Dimension(900, 450));
		setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		scrollPane.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		add(scrollPane);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		
		panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel.setMinimumSize(new Dimension(220, 10));
		panel.setPreferredSize(new Dimension(220, 10));
		add(panel, BorderLayout.EAST);
		panel.setLayout(null);
		
		lblNewLabel = new JLabel("Nazwisko");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setMinimumSize(new Dimension(60, 20));
		lblNewLabel.setPreferredSize(new Dimension(60, 20));
		lblNewLabel.setBounds(10, 11, 80, 20);
		panel.add(lblNewLabel);
		
		comboBox = new JComboBox();
		comboBox.setEditable(true);
		comboBox.setBounds(100, 11, 110, 20);
		panel.add(comboBox);
		
		lblNewLabel_1 = new JLabel("Imi\u0119");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(10, 35, 80, 20);
		panel.add(lblNewLabel_1);
		
		comboBox_1 = new JComboBox();
		comboBox_1.setEditable(true);
		comboBox_1.setBounds(100, 35, 110, 20);
		panel.add(comboBox_1);
		
		lblNewLabel_2 = new JLabel("<html><p text-align: right>Stracone<br>      \r\n<p text-align: right>bramki</html>");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setBounds(10, 105, 80, 34);
		panel.add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("Data urodzenia");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_3.setBounds(10, 58, 80, 20);
		panel.add(lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("Klub");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_4.setBounds(10, 85, 80, 20);
		panel.add(lblNewLabel_4);
		
		comboBox_3 = new JComboBox();
		comboBox_3.setEditable(true);
		comboBox_3.setBounds(100, 85, 110, 20);
		panel.add(comboBox_3);
		
		btnNewButton = new JButton("Filtruj\r\n");
		btnNewButton.setIcon(new ImageIcon(GolKipperView.class.getResource("/program/resources/filters16.png")));
		btnNewButton.setBounds(100, 181, 110, 23);
		panel.add(btnNewButton);
		
		calendarCombo = new JCalendarCombo();
		calendarCombo.setForeground(Color.BLACK);
		calendarCombo.setBackground(Color.WHITE);
		calendarCombo.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM));
		calendarCombo.setBounds(100, 60, 110, 20);
		panel.add(calendarCombo);
		
		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(0, 0, 20, 1));
		spinner.setBounds(100, 110, 110, 20);
		panel.add(spinner);
	}
}
