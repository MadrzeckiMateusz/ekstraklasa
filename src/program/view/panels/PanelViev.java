package program.view.panels;

import javax.swing.JPanel;
import javax.swing.Box;
import javax.swing.JComboBox;

import java.awt.Dimension;
import java.awt.BorderLayout;

import net.miginfocom.swing.MigLayout;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;

import program.controler.panels.panelControler;

import java.awt.Component;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.util.Date;

import org.freixas.jcalendar.JCalendarCombo;

import javax.swing.DefaultComboBoxModel;
import java.awt.Checkbox;
import java.awt.Point;
import javax.swing.border.BevelBorder;
import javax.swing.ImageIcon;

public class PanelViev extends JPanel {
	public JPanel filterArea;
	public JButton btnNewButton;
	public JLabel lblNewLabel_2;
	public JComboBox<String> comboBox_1;
	public JLabel lblNewLabel_1;
	public JLabel lblNewLabel;
	public JComboBox<String> comboBox;
	public JTable table;
	public JCalendarCombo calendarCombo;
	public JScrollPane scrollPane;
	
	public PanelViev() {		
		
		setPreferredSize(new Dimension(900, 450));
		setMinimumSize(new Dimension(900, 450));
		setLayout(new BorderLayout(0, 0));
		
		filterArea = new JPanel();
		filterArea.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		filterArea.setMinimumSize(new Dimension(220, 10));
		filterArea.setPreferredSize(new Dimension(220, 35));
		add(filterArea, BorderLayout.EAST);
		filterArea.setLayout(null);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(100, 5, 110, 20);
		filterArea.add(comboBox);
		
		lblNewLabel = new JLabel("Dru\u017Cyny:");
		lblNewLabel.setBounds(10, 5, 80, 20);
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setPreferredSize(new Dimension(40, 20));
		filterArea.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Nr kolejki:");
		lblNewLabel_1.setBounds(10, 30, 80, 20);
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		filterArea.add(lblNewLabel_1);
		
		comboBox_1 = new JComboBox<String>();
		comboBox_1.setBounds(100, 30, 110, 20);
		filterArea.add(comboBox_1);
		
		lblNewLabel_2 = new JLabel("Data spotkania:\r\n");
		lblNewLabel_2.setBounds(10, 55, 80, 20);
		filterArea.add(lblNewLabel_2);
		
		btnNewButton = new JButton("Filtruj");
		btnNewButton.setIcon(new ImageIcon(PanelViev.class.getResource("/program/resources/filters16.png")));
		btnNewButton.setBounds(100, 90, 110, 20);
		filterArea.add(btnNewButton);
		
		calendarCombo = new JCalendarCombo();
		calendarCombo.setBounds(100, 55, 110, 20);
		calendarCombo.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM));
		filterArea.add(calendarCombo);
		
		scrollPane = new JScrollPane();
		scrollPane.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
	}
}
