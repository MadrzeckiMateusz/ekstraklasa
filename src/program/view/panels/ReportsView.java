package program.view.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.DateFormat;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JSpinner;

import org.freixas.jcalendar.JCalendarCombo;
import javax.swing.SpinnerNumberModel;
import javax.swing.ImageIcon;

public class ReportsView extends JPanel {
	public JTable table;
	public JScrollPane scrollPane;
	public JPanel panel;
	public JPanel panel_1;
	public JPanel panel_2;
	public JButton btnNewButton;
	public JCalendarCombo calendarCombo;
	public JSpinner spinner;
	public JComboBox comboBox_1;
	public JComboBox comboBox;
	public JLabel lblNewLabel_3;
	public JLabel lblNewLabel_2;
	public JLabel lblNewLabel_1;
	public JLabel lblNewLabel;
	public JButton btnNewButton_1;
	public ReportsView() {
		setPreferredSize(new Dimension(900, 450));
		setMinimumSize(new Dimension(900, 450));
		setLayout(new BorderLayout(0, 0));
		
		panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		scrollPane.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel.add(scrollPane);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		table.setAutoCreateRowSorter(true);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setRowSelectionAllowed(true);
		table.setColumnSelectionAllowed(false);
		scrollPane.setViewportView(table);
		
		panel_1 = new JPanel();
		panel_1.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_1.setPreferredSize(new Dimension(220, 10));
		panel_1.setMinimumSize(new Dimension(220, 10));
		add(panel_1, BorderLayout.EAST);
		panel_1.setLayout(null);
		
		lblNewLabel = new JLabel("Nazwisko");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(5, 11, 80, 20);
		panel_1.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Imi\u0119");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(5, 35, 80, 20);
		panel_1.add(lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("Numer s\u0119dziego");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setBounds(5, 60, 80, 20);
		panel_1.add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("Data urodzenia");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_3.setBounds(5, 85, 80, 20);
		panel_1.add(lblNewLabel_3);
		
		comboBox = new JComboBox();
		comboBox.setEditable(true);
		comboBox.setBounds(100, 11, 110, 20);
		panel_1.add(comboBox);
		
		comboBox_1 = new JComboBox();
		comboBox_1.setEditable(true);
		comboBox_1.setBounds(100, 35, 110, 20);
		panel_1.add(comboBox_1);
		
		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinner.setBounds(100, 60, 110, 20);
		panel_1.add(spinner);
		
		calendarCombo = new JCalendarCombo();
		calendarCombo.setBounds(100, 85, 110, 20);
		calendarCombo.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM));
		panel_1.add(calendarCombo);
		
		btnNewButton_1 = new JButton("Filtruj");
		btnNewButton_1.setIcon(new ImageIcon(ReportsView.class.getResource("/program/resources/filters16.png")));
		btnNewButton_1.setBounds(100, 162, 110, 23);
		panel_1.add(btnNewButton_1);
		
		panel_2 = new JPanel();
		panel_2.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_2.setPreferredSize(new Dimension(10, 40));
		panel_2.setMinimumSize(new Dimension(10, 40));
		add(panel_2, BorderLayout.NORTH);
		panel_2.setLayout(null);
		
		btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(ReportsView.class.getResource("/program/resources/more24.png")));
		btnNewButton.setToolTipText("Wy\u015Bwietl szczeg\u00F3\u0142y");
		btnNewButton.setBounds(10, 5, 30, 30);
		panel_2.add(btnNewButton);
	}
}
