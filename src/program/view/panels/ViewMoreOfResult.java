package program.view.panels;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Enumeration;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DebugGraphics;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.Window.Type;

public class ViewMoreOfResult extends JDialog {
	public JScrollPane scrollPane;
	public JTable table;
	private Dimension DimensionOfFrame;
	private Dimension DimensionOfScreenSize;
	public ViewMoreOfResult() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ViewMoreOfResult.class.getResource("/program/resources/more24.png")));
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		setType(Type.POPUP);
		setModal(true);
		setMinimumSize(new Dimension(700, 300));
		setPreferredSize(new Dimension(200, 0));
		setTitle("Szczeg\u00F3\u0142y");
		setModalityType(ModalityType.APPLICATION_MODAL);
		CenterFrame();
		
		scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setPreferredScrollableViewportSize(new Dimension(500, 400));
		table.setFillsViewportHeight(true);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		scrollPane.setViewportView(table);
		scrollPane.setAutoscrolls(true);
	}
	
	 public void CenterFrame(){
		 this.DimensionOfFrame = getSize();		
			this.DimensionOfScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
			if (DimensionOfFrame.height > this.DimensionOfScreenSize.height){
				DimensionOfFrame.height = DimensionOfScreenSize.height;
			}
			if (DimensionOfFrame.width > DimensionOfScreenSize.width){
				DimensionOfFrame.width = DimensionOfScreenSize.width;
			}
			
			setLocation((DimensionOfScreenSize.width - DimensionOfFrame.width) / 2, 
				      (DimensionOfScreenSize.height - DimensionOfFrame.height) / 2);
	 }

}
