package program.view.panels;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.border.BevelBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;

public class DataBaseManagmentView extends JPanel {
	public JTable table;
	public JPanel panel;
	public JPanel panel_2;
	public JScrollPane scrollPane;
	public JPanel panel_3;
	public JPanel panel_1;
	public JTextField textField;
	public JLabel lblNewLabel_2;
	public JSeparator separator_1;
	public JButton btnNewButton_3;
	public JComboBox comboBox;
	public JLabel lblNewLabel_1;
	public JSeparator separator;
	public JLabel lblNewLabel;
	public JButton btnNewButton;
	public JButton btnNewButton_1;
	public JButton btnNewButton_2;
	public DataBaseManagmentView() {
		setPreferredSize(new Dimension(900, 450));
		setMinimumSize(new Dimension(900, 450));
		setLayout(new BorderLayout(0, 0));
		
		panel = new JPanel();
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		panel_2 = new JPanel();
		panel_2.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		panel_2.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		
		panel_3 = new JPanel();
		panel_3.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_3.setPreferredSize(new Dimension(10, 40));
		panel_3.setMinimumSize(new Dimension(10, 40));
		panel.add(panel_3, BorderLayout.NORTH);
		panel_3.setLayout(null);
		
		btnNewButton = new JButton("");
		btnNewButton.setToolTipText("Dodaj");
		btnNewButton.setBorder(null);
		btnNewButton.setIcon(new ImageIcon(DataBaseManagmentView.class.getResource("/program/resources/add16.png")));
		btnNewButton.setBounds(10, 5, 30, 30);
		panel_3.add(btnNewButton);
		
		btnNewButton_1 = new JButton("");
		btnNewButton_1.setToolTipText("Edycja ");
		btnNewButton_1.setIcon(new ImageIcon(DataBaseManagmentView.class.getResource("/program/resources/edit24.png")));
		btnNewButton_1.setBounds(50, 5, 30, 30);
		panel_3.add(btnNewButton_1);
		
		btnNewButton_2 = new JButton("");
		btnNewButton_2.setToolTipText("Usu\u0144");
		btnNewButton_2.setIcon(new ImageIcon(DataBaseManagmentView.class.getResource("/program/resources/delete24.png")));
		btnNewButton_2.setBounds(90, 5, 30, 30);
		panel_3.add(btnNewButton_2);
		
		panel_1 = new JPanel();
		panel_1.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_1.setPreferredSize(new Dimension(220, 10));
		panel_1.setMinimumSize(new Dimension(220, 10));
		add(panel_1, BorderLayout.EAST);
		panel_1.setLayout(null);
		
		lblNewLabel = new JLabel("Baza danych");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 10, 80, 20);
		panel_1.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(100, 10, 110, 20);
		panel_1.add(textField);
		textField.setColumns(10);
		
		separator = new JSeparator();
		separator.setBounds(0, 39, 234, 2);
		panel_1.add(separator);
		
		lblNewLabel_1 = new JLabel("Wybierz tabele");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(10, 50, 80, 20);
		panel_1.add(lblNewLabel_1);
		
		comboBox = new JComboBox();
		comboBox.setBounds(100, 50, 110, 20);
		panel_1.add(comboBox);
		
		btnNewButton_3 = new JButton("Wybierz");
		btnNewButton_3.setIcon(new ImageIcon(DataBaseManagmentView.class.getResource("/program/resources/select16.png")));
		btnNewButton_3.setBounds(100, 81, 110, 20);
		panel_1.add(btnNewButton_3);
		
		separator_1 = new JSeparator();
		separator_1.setBounds(0, 112, 220, 2);
		panel_1.add(separator_1);
		
		lblNewLabel_2 = new JLabel("Filtry");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(70, 120, 80, 20);
		panel_1.add(lblNewLabel_2);
	}
}
