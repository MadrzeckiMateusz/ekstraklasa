package program.view.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class UpdateDataView extends JDialog {
	public JPanel panel;
	public JButton Cancel;
	public JButton Update;
	private JSeparator separator;
	public UpdateDataView() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(UpdateDataView.class.getResource("/program/resources/edit48.png")));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setMinimumSize(new Dimension(400, 400));
		setPreferredSize(new Dimension(400, 400));
		setVisible(true);
		
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		Update = new JButton("Dodaj");
		Update.setIcon(new ImageIcon(UpdateDataView.class.getResource("/program/resources/yes16.png")));
		Update.setBounds(221, 327, 102, 23);
		panel.add(Update);
		
		Cancel = new JButton("Cancel");
		Cancel.setIcon(new ImageIcon(UpdateDataView.class.getResource("/program/resources/no16.png")));
		Cancel.setBounds(70, 327, 102, 23);
		panel.add(Cancel);
		
		separator = new JSeparator();
		separator.setBounds(0, 314, 384, 2);
		panel.add(separator);
}
}
