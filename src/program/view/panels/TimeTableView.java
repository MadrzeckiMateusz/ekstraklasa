package program.view.panels;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.border.BevelBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.freixas.jcalendar.JCalendarCombo;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.border.EtchedBorder;
import javax.swing.ImageIcon;

public class TimeTableView extends JPanel {
	public JPanel panel_1;
	public JPanel panel;
	public JTable table;
	public JScrollPane scrollPane;
	public JLabel lblNewLabel;
	public JComboBox comboBox;
	public JLabel lblNewLabel_1;
	public JSpinner spinner;
	public  JLabel lblNewLabel_2;
	public JCalendarCombo calendarCombo;
	public JLabel lblNewLabel_3;
	public JCalendarCombo calendarCombo_1;
	public JButton btnNewButton;
	public TimeTableView() {
		setPreferredSize(new Dimension(900, 450));
		setMinimumSize(new Dimension(900, 450));
		setLayout(new BorderLayout(0, 0));
		
		panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		scrollPane.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		panel.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		
		panel_1 = new JPanel();
		panel_1.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_1.setPreferredSize(new Dimension(220, 10));
		panel_1.setMinimumSize(new Dimension(220, 10));
		add(panel_1, BorderLayout.EAST);
		panel_1.setLayout(null);
		
		lblNewLabel = new JLabel("Dru\u017Cyna");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 11, 80, 20);
		panel_1.add(lblNewLabel);
		
		comboBox = new JComboBox();
		comboBox.setEditable(true);
		comboBox.setBounds(100, 11, 110, 20);
		panel_1.add(comboBox);
		
		lblNewLabel_1 = new JLabel("Kolejka");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(10, 36, 80, 20);
		panel_1.add(lblNewLabel_1);
		
		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(0, 0, 21, 1));
		spinner.setBounds(100, 36, 110, 20);
		panel_1.add(spinner);
		
		lblNewLabel_2 = new JLabel("Data od");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setBounds(10, 61, 80, 20);
		panel_1.add(lblNewLabel_2);
		
		calendarCombo = new JCalendarCombo();
		calendarCombo.setBounds(100, 61, 110, 20);
		calendarCombo.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM));
		panel_1.add(calendarCombo);
		
		lblNewLabel_3 = new JLabel("Data do");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_3.setBounds(10, 86, 80, 20);
		panel_1.add(lblNewLabel_3);
		
		calendarCombo_1 = new JCalendarCombo();
		calendarCombo_1.setBounds(100, 86, 110, 20);
		calendarCombo_1.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM));
		panel_1.add(calendarCombo_1);
		
		btnNewButton = new JButton("Filtruj");
		btnNewButton.setIcon(new ImageIcon(TimeTableView.class.getResource("/program/resources/filters16.png")));
		btnNewButton.setBounds(100, 148, 110, 20);
		panel_1.add(btnNewButton);
	}
}
