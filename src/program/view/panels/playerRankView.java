package program.view.panels;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.JButton;

public class playerRankView extends JPanel {
	public JTable table;
	public JScrollPane scrollPane;
	public JButton king;
	public JPanel panel;
	
	
	public playerRankView() {
		setPreferredSize(new Dimension(900, 450));
		setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(10, 40));
		panel.setMinimumSize(new Dimension(10, 40));
		panel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		add(panel, BorderLayout.NORTH);
		panel.setLayout(null);
		
		king = new JButton("Kr\u00F3l strzelc\u00F3w");
		king.setBounds(10, 5, 104, 30);
		panel.add(king);
	}

}
