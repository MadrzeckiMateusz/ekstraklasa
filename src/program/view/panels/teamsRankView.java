package program.view.panels;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.JButton;

public class teamsRankView extends JPanel {
	public JScrollPane scrollPane;
	public JTable table;
	public JButton avgwiek;
	public teamsRankView() {
		setPreferredSize(new Dimension(900, 450));
		setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		
		JPanel panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel.setPreferredSize(new Dimension(10, 40));
		panel.setMinimumSize(new Dimension(10, 40));
		add(panel, BorderLayout.NORTH);
		panel.setLayout(null);
		
		avgwiek = new JButton("\u015Aredni wiek");
		avgwiek.setBounds(10, 11, 89, 23);
		panel.add(avgwiek);
	}
}
