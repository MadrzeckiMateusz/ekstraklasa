package program.view.panels;

import javax.swing.JDialog;
import java.awt.Dimension;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.ImageIcon;
import javax.swing.JSeparator;
import java.awt.Toolkit;

public class AddDataView extends JDialog {
	public JPanel panel;
	public JButton Cancel;
	public JButton add;
	public AddDataView() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(AddDataView.class.getResource("/program/resources/edit48.png")));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setMinimumSize(new Dimension(400, 400));
		setPreferredSize(new Dimension(400, 400));
		setVisible(true);
		
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		add = new JButton("Akceptuj");
		add.setIcon(new ImageIcon(AddDataView.class.getResource("/program/resources/yes16.png")));
		add.setBounds(212, 327, 102, 23);
		panel.add(add);
		
		Cancel = new JButton("Anuluj");
		Cancel.setIcon(new ImageIcon(AddDataView.class.getResource("/program/resources/no16.png")));
		Cancel.setBounds(70, 327, 102, 23);
		panel.add(Cancel);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 314, 384, 2);
		panel.add(separator);
	}
}
