package program.view.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.ImageIcon;

public class roundView extends JPanel {
	
	
	public JPanel filterArea;
	public JScrollPane scrollPane;
	public JTable table;
	public JLabel lblNewLabel;
	public JComboBox comboBox;
	public JButton btnNewButton;
	
	public roundView() {
		setPreferredSize(new Dimension(900, 450));
		setLayout(new BorderLayout(1, 1));
		
		scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		
		filterArea = new JPanel();
		filterArea.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		filterArea.setMinimumSize(new Dimension(220, 35));
		filterArea.setPreferredSize(new Dimension(220, 35));
		add(filterArea, BorderLayout.EAST);
		filterArea.setLayout(null);
		
		lblNewLabel = new JLabel("Kolejka: ");
		lblNewLabel.setBounds(10, 5, 80, 20);
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setPreferredSize(new Dimension(46, 20));
		filterArea.add(lblNewLabel);
		
		comboBox = new JComboBox();
		comboBox.setBounds(100, 5, 110, 20);
		filterArea.add(comboBox);
		
		btnNewButton = new JButton("Filtruj");
		btnNewButton.setIcon(new ImageIcon(roundView.class.getResource("/program/resources/filters16.png")));
		btnNewButton.setBounds(100, 35, 110, 20);
		btnNewButton.setPreferredSize(new Dimension(59, 20));
		filterArea.add(btnNewButton);
	}
}
