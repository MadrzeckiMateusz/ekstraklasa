package program.view.panels;

import javax.swing.JPanel;
import java.awt.Dimension;
import javax.swing.border.BevelBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class StatusBarView extends JPanel{
	public JTextField textField;
	public StatusBarView() {
		setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		setPreferredSize(new Dimension(500, 40));
		setMinimumSize(new Dimension(500, 40));
		setLayout(null);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(70, 11, 420, 20);
		add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Info");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 10, 50, 20);
		add(lblNewLabel);
	}
}
