package program.view.window;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import java.awt.Window.Type;

public class LoginWindow extends JFrame {
	public JLabel lblUytkownik;
	public JPasswordField passwordField;
	public JButton btnNewButton;
	public JButton btnNewButton_1;
	public JLabel lblNewLabel;
	public JComboBox comboBox;
	private Dimension DimensionOfFrame;
	private Dimension DimensionOfScreenSize;
	public LoginWindow() {
		setResizable(false);
		setType(Type.UTILITY);
		setIconImage(Toolkit.getDefaultToolkit().getImage(LoginWindow.class.getResource("/program/resources/Soccer-Ball-icon48.png")));
		
		setMinimumSize(new Dimension(274, 174));
		setPreferredSize(new Dimension(274, 174));
		CenterFrame();
		setTitle("Ekstraklasa-Logowanie");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);
		
		lblUytkownik = new JLabel("U\u017Cytkownik ");
		lblUytkownik.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUytkownik.setBounds(30, 26, 67, 20);
		getContentPane().add(lblUytkownik);
		
		comboBox = new JComboBox();
		comboBox.setEditable(true);
		comboBox.setBounds(103, 26, 129, 20);
		getContentPane().add(comboBox);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(103, 57, 129, 20);
		getContentPane().add(passwordField);
		
		lblNewLabel = new JLabel("Has\u0142o");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(30, 57, 67, 20);
		getContentPane().add(lblNewLabel);
		
		btnNewButton = new JButton("Zamknij");
		btnNewButton.setBounds(30, 99, 89, 23);
		getContentPane().add(btnNewButton);
		
		btnNewButton_1 = new JButton("Zaloguj");
		btnNewButton_1.setBounds(143, 99, 89, 23);
		getContentPane().add(btnNewButton_1);
	}
	
	 public void CloseWindow()
		{
			int a = JOptionPane.showConfirmDialog(this, "Czy zako�czy� program ?",
					"Ko�czenie pracy programu",JOptionPane.OK_CANCEL_OPTION );
					if (a == JOptionPane.OK_OPTION )
			{
			
				System.exit(0);
			}
			else
				{
					return;
				}
		}
	 public void CenterFrame(){
		 this.DimensionOfFrame = getSize();		
			this.DimensionOfScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
			if (DimensionOfFrame.height > this.DimensionOfScreenSize.height){
				DimensionOfFrame.height = DimensionOfScreenSize.height;
			}
			if (DimensionOfFrame.width > DimensionOfScreenSize.width){
				DimensionOfFrame.width = DimensionOfScreenSize.width;
			}
			
			setLocation((DimensionOfScreenSize.width - DimensionOfFrame.width) / 2, 
				      (DimensionOfScreenSize.height - DimensionOfFrame.height) / 2);
	 }
}
