package program.view.window;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import program.controler.panels.DataBaseManagmentControler;
import program.controler.panels.GolKipperControler;
import program.controler.panels.ReportsControler;
import program.controler.panels.StatusBarControler;
import program.controler.panels.TimeTableControler;
import program.controler.panels.panelControler;
import program.controler.panels.playerRankControler;
import program.controler.panels.roundControler;
import program.controler.panels.teamsRankControler;

import javax.swing.ImageIcon;
import javax.swing.border.BevelBorder;
import javax.swing.JTextField;
import javax.swing.JMenuItem;


public class mainWindow extends JFrame{
	public JPanel mainPanel;
	public JTabbedPane horizontalTabs;
	public JPanel results;
	public JMenuBar menuBar;
	public JMenu mnPlik;
	public JMenu mnEdycja;
	public JMenu mnPomoc;
	public JTabbedPane verticalResultsTabs;
	public JPanel rankings;

	
	public JTabbedPane verticalRanksTabs;
	public JPanel teamsRank;
	public JPanel golkiperRank;
	public JPanel timeTable;
	public JTabbedPane verticalTimeTableTabs;
	public JPanel reportsPZPN;
	public JPanel matchTimeTable;
	public JPanel roundsTime;
	public JTabbedPane verticalReportsTabs;
	public JPanel arbiterReports;
	public JPanel managmentDataBase;
	public JPanel panel;
	
	private panelControler pControler;
	private roundControler rControler;
	private teamsRankControler trControler;
	private playerRankControler pRankControler;
	private GolKipperControler golKipperControler;
	private TimeTableControler timeTableControler;
	private ReportsControler reportsControler;
	private DataBaseManagmentControler dataBaseManagmentControler;
	private StatusBarControler statusBarControler;
	
	private Dimension DimensionOfFrame;
	private Dimension DimensionOfScreenSize;
	public JPanel panel_1;
	public JMenuItem Close;
	public JMenuItem logout;
	
	public mainWindow() {
		setTitle("EkstraKlasa");
		setIconImage(Toolkit.getDefaultToolkit().getImage(mainWindow.class.getResource("/program/resources/Soccer-Ball-icon48.png")));
		statusBarControler = new StatusBarControler();
		pControler = new panelControler();
		rControler = new roundControler();
		trControler = new teamsRankControler();
		pRankControler = new playerRankControler();
		golKipperControler = new GolKipperControler();
		timeTableControler = new TimeTableControler();
		reportsControler = new ReportsControler();
		dataBaseManagmentControler = new DataBaseManagmentControler();
		
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setMinimumSize(new Dimension(1000, 600));
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		CenterFrame();
		mainPanel = new JPanel();
		getContentPane().add(mainPanel, BorderLayout.CENTER);
		mainPanel.setLayout(new BorderLayout(0, 0));
		
		horizontalTabs = new JTabbedPane(JTabbedPane.LEFT);
		horizontalTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		mainPanel.add(horizontalTabs, BorderLayout.CENTER);
		
		results = new JPanel();
		horizontalTabs.addTab("Wyniki", new ImageIcon(mainWindow.class.getResource("/program/resources/result32.png")), results, null);
		
		results.setLayout(new BorderLayout(0, 0));
		
		verticalResultsTabs = new JTabbedPane(JTabbedPane.TOP);
		results.add(verticalResultsTabs);
		verticalResultsTabs.addTab("<html><p text-align: right>Spotka�<br><br>\r\n</html>", null, pControler.getView(), null);			
		verticalResultsTabs.addTab("Kolejki", null, rControler.getrView(), null);
		
		rankings = new JPanel();
		horizontalTabs.addTab("Rankingi", new ImageIcon(mainWindow.class.getResource("/program/resources/rating.png")), rankings, null);
		rankings.setLayout(new BorderLayout(0, 0));
		
		verticalRanksTabs = new JTabbedPane(JTabbedPane.TOP);
		rankings.add(verticalRanksTabs);
		
		
		verticalRanksTabs.addTab("Dru\u017Cyn", null, trControler.getTrView(), null);
		verticalRanksTabs.addTab("Pi\u0142karze", null,pRankControler.getpRankView() , null);
		verticalRanksTabs.addTab("Bramkarze", null, golKipperControler.getGolKipperView(), null);
		
		
		horizontalTabs.addTab("Harmonogram",new ImageIcon(mainWindow.class.getResource("/program/resources/calendar32.png")), timeTableControler.getTtView(), null);
		
		reportsPZPN = new JPanel();
		horizontalTabs.addTab("Raporty", new ImageIcon(mainWindow.class.getResource("/program/resources/reports32.png")), reportsPZPN, null);
		reportsPZPN.setLayout(new BorderLayout(0, 0));
		
		verticalReportsTabs = new JTabbedPane(JTabbedPane.TOP);
		reportsPZPN.add(verticalReportsTabs);
		
		
		verticalReportsTabs.addTab("Praca S\u0119dziego", null, reportsControler.getRpView(), null);
		
		managmentDataBase = new JPanel();
		managmentDataBase.setEnabled(false);
		managmentDataBase.setVisible(false);
		horizontalTabs.addTab("Zarz\u0105dzanie Baz\u0105", new ImageIcon(mainWindow.class.getResource("/program/resources/managemet.png")), managmentDataBase, null);
		managmentDataBase.setLayout(new BorderLayout(0, 0));
		
		
		managmentDataBase.add(dataBaseManagmentControler.getDBMView(), BorderLayout.CENTER);
		
		menuBar = new JMenuBar();
		getContentPane().add(menuBar, BorderLayout.NORTH);
		
		mnPlik = new JMenu("Plik");
		menuBar.add(mnPlik);
		
		logout = new JMenuItem("Wyloguj");
		mnPlik.add(logout);
		
		Close = new JMenuItem("Zamknij");
		mnPlik.add(Close);
		
		mnEdycja = new JMenu("Edycja");
		menuBar.add(mnEdycja);
		
		mnPomoc = new JMenu("Pomoc");
		menuBar.add(mnPomoc);
		
		panel_1 = new JPanel();
		panel_1.setPreferredSize(new Dimension(10, 40));
		panel_1.setMinimumSize(new Dimension(10, 40));
		getContentPane().add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		panel_1.add(statusBarControler.getStBView(),BorderLayout.CENTER);
		
	}
	
	 public void CenterFrame(){
		 this.DimensionOfFrame = getSize();		
			this.DimensionOfScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
			if (DimensionOfFrame.height > this.DimensionOfScreenSize.height){
				DimensionOfFrame.height = DimensionOfScreenSize.height;
			}
			if (DimensionOfFrame.width > DimensionOfScreenSize.width){
				DimensionOfFrame.width = DimensionOfScreenSize.width;
			}
			
			setLocation((DimensionOfScreenSize.width - DimensionOfFrame.width) / 2, 
				      (DimensionOfScreenSize.height - DimensionOfFrame.height) / 2);
	 }
}
